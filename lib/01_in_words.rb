class Fixnum

def in_words(int=self)
  string_num = int.to_s
  string_num.delete!("_")
  result = ""
  return "zero" if int == 0
  ones_hash = {9 => 'nine',
      8 => "eight",
      7 => "seven",
      6 => "six",
      5 => "five",
      4 => "four",
      3 => "three",
      2 => "two",
      1 => 'one',
  }

  teens_hash = {19=>"nineteen",
      18=>"eighteen",
      17=>"seventeen",
      16=>"sixteen",
      15=>"fifteen",
      14=>"fourteen",
      13=>"thirteen",
      12=>"twelve",
      11 => "eleven",
      10 => "ten"
  }

  tens_hash = {9 => "ninety",
      8 => "eighty",
      7 => "seventy",
      6 => "sixty",
      5 => "fifty",
      4 => "forty",
      3 => "thirty",
      2 => "twenty"
  }

  string_num.each_char.with_index do |char,index|

    current_value = char.to_i
    previous_value = string_num[index-1] unless index == 0
    next_value = string_num[index+1] unless index == string_num.length-1

    dist_to_end = string_num.length-index-1

    #hundreds digit
    if dist_to_end % 3 == 2
      next if current_value == 0
      result += " " + ones_hash[current_value] + " hundred"
    #tens digit
    elsif dist_to_end % 3 == 1
      next if current_value == 0
      if current_value == 1
        result += " " + teens_hash[(current_value.to_s + next_value).to_i]
      else
        result += " " + tens_hash[current_value]
      end
    #ones digit
    elsif dist_to_end % 3 == 0
      result += " " + ones_hash[current_value] unless previous_value == "1" || current_value == 0
      next if string_num[index] == "0" && string_num[index-1] == "0" && string_num[index-2] == "0"
      if dist_to_end / 3 == 1
        result += " thousand"
      elsif dist_to_end / 3 == 2
        result += " million"
      elsif dist_to_end / 3 == 3
        result += " billion"
      elsif dist_to_end / 3 == 4
        result += " trillion"
      end
    end
  end
  result[1..result.length-1]
end

end
